Setup on Debian "jessie" stable
-------------------------------

We're going to install pump.io from the npm registry, use MongoDB for
the database and proxy pump.io behind the nginx web server.

Install basic requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~

Install the requirements via the Debian package manager.

::

    $ sudo apt-get install nodejs-legacy mongodb nginx graphicsmagick git

Debian jessie provides nodejs of version 0.10, while pump.io after version
2.0.0 beta 1 requires nodejs 4.x. Therefore, it must be updated from upstream.

::

    $ curl -sL https://deb.nodesource.com/setup_4.x | sudo bash -
    $ sudo apt-get install -y nodejs

Install or fix npm
~~~~~~~~~

The version of npm in Debian jessie is very old. Therefore, it's
recommended that you install npm directly from upstream and uninstall
the packaged version.

.. NOTE:: While installing from source instead of the Debian
   repositories usually isn't a good idea, it is safe to do in the
   case of npm, for a couple reasons:

   1. npm is a package manager itself, so it's good at keeping track
      of things it installs (including itself).
   2. npm backward-compatibility is very good. npm v3 does not
      function radically differently than npm v1, which is what is in
      Debian.
   3. npm is quite stable.

First, check if you even have npm installed:

::

    $ type npm
    npm is /usr/bin/npm

If you see ``npm not found`` instead, install npm with ``curl -L
https://npmjs.org/install.sh | sudo sh`` (or equivalent) and move on
to "Install and set up pump.io."

If not, check if you have the packaged version installed:

::

    $ apt-cache policy npm
    npm:
      Installed: (none)
      Candidate: 1.4.21+ds-2
      Version table:
         1.4.21+ds-2 0
            500 http://ftp.us.debian.org/debian/ jessie/main amd64 Packages
            100 /var/lib/dpkg/status

If you see output similar to the above, that means you have npm
installed from upstream. Move on to "Install and set up pump.io."

If instead you see something similar to ``Installed: 1.4.21+ds-2``,
you need to remove npm and install from upstream. You can do this with:

::

    $ sudo apt remove npm
    $ sudo apt-get autoremove # Optional
    $ sudo rm -r /usr/local/lib/node_modules # Removes ALL npm packages!
    $ curl -L https://npmjs.org/install.sh > npm-install.sh
    $ sudo sh npm-install.sh

.. WARNING:: Running ``sudo rm -r /usr/local/lib/node_modules`` will
    remove all currently-installed npm packages. Blowing away
    everything already installed is optional but strongly recommended
    as npm v1 is prone to creating broken, half-installed directory
    structures that interfere with future installations. In addition,
    npm in Debian has a default install path of
    ``/usr/local/lib/node_modules``, whereas upstream installs to
    ``/usr/lib/node_modules``. Simply removing the former path makes
    everything much less confusing.

    If you want to keep your currently-installed packages the best way
    to do so is to save the list of installed packages, proceed with
    the removal, then reinstall using npm v3. Conveniently, you can
    get a list of installed packages by doing ``ls
    /usr/local/lib/node_modules``.

If you'd like, you can inspect the contents of ``npm-install.sh``
before running ``sudo sh npm-install.sh``.

Congrats! You now have an npm installed from upstream.

Install and set up pump.io
~~~~~~~~~~~~~~~~~~~~~~~~~~

Next, we create a ``pumpio`` user and group and create the needed folders:

::

    $ sudo groupadd pumpio
    $ sudo useradd -d /var/lib/pumpio -m -r -g pumpio pumpio

    $ sudo mkdir -p /srv/pumpio/uploads
    $ sudo chown -R pumpio:pumpio /srv/pumpio

    $ sudo mkdir /var/log/pumpio/
    $ sudo chown pumpio:pumpio /var/log/pumpio/

Next, install pump.io itself, along with the MongoDB Databank driver:

::

    $ sudo npm install -g pump.io databank-mongodb

Finally pump.io needs a configuration file in
``/var/lib/pumpio/.pump.io.json`` with the following content (adapted to
your situation):

::

    {
        "driver":  "mongodb",
        "params": {"host":"localhost","dbname":"pumpio"},
        "hostname":  "your.pump.com",
        "address": "127.0.0.1",
        "port": 8080,
        "urlPort": 443,
        "secret":  "somerandomstringhere",
        "key":  "/path/to/your/ssl-cert.crt",
        "cert":  "/path/to/your/ssl-cert.key",
        "noweb":  false,
        "site":  "your.pump.com",
        "owner":  "Your Name",
        "ownerURL":  "http://your.site.com/",
        "nologger": false,
        "logfile": "/var/log/pumpio/pumpio.log",
        "serverUser":  "pumpio",
        "uploaddir": "/srv/pumpio/uploads",
        "debugClient": false,
        "firehose": "ofirehose.com",
        "disableRegistration": true,
        "noCDN": true,
        "requireEmail": false,
        "compress": true,
        "smtpserver": "localhost",
        "proxyWhitelist": ["avatar3.status.net", "avatar.identi.ca", "secure.gravatar.com"]
    }

In particular you need to replace ``your.pump.com`` which your actual
domain name.

Make sure that the pumpio user can access your SSL certs.

You can now try and see if it works:

::

    $ sudo -u pumpio pump

This starts up pump.io running on port 8080, this will be proxied
behind nginx which will serve it on port 443 (https).

Setup nginx proxying
~~~~~~~~~~~~~~~~~~~~

Create the file ``/etc/nginx/sites-available/pump`` with the following
content (adapted to your situation):

::

    upstream pumpiobackend {
      server 127.0.0.1:8080 max_fails=3;
    }

    server {
      server_name your.pump.com;
      rewrite ^ https://your.pump.com$request_uri?;
    }

    server {
      listen 443 ssl;
      server_name your.pump.com;

      ssl_certificate  /path/to/your/ssl-cert.crt;
      ssl_certificate_key  /path/to/your/ssl-cert.key;

      access_log /var/log/nginx/pumpio.access.log;
      error_log /var/log/nginx/pumpio.error.log;

      client_max_body_size 500m;

      keepalive_timeout 75 75;
      gzip_vary off;

      location / {
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Real-IP $remote_addr;
    
        proxy_redirect off;
    
        proxy_buffers 16 32k;
        proxy_cache off;
        proxy_connect_timeout 60s;
        proxy_read_timeout 60s;
        proxy_pass https://pumpiobackend;

        proxy_pass_header Server;
      }
    }

You will of course have to provide your own SSL cert and put them in
the right path. Make sure that the certificate key is not world
readable!

Finally you can link the file you created to enable it (in a root
shell):

::

    # cd /etc/nginx/sites-enabled
    # ln -s ../sites-available/pump .

If everything is set up correctly you should now be able to restart
nginx and access the site in your web browser:

::

    $ sudo systemctl restart nginx

Before you start using your site make sure have settled on the correct
hostname. You can't switch that around later without breaking
federation badly.

Start pump.io automatically using systemd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To have pump.io start automatically just create the file
``/etc/systemd/system/pump.service`` with the following content:

::

    [Unit]
    Description=pump.io
    After=syslog.target network.target

    [Service]
    Type=simple
    User=pumpio
    Group=pumpio
    ExecStart=/usr/bin/pump

    [Install]
    WantedBy=multi-user.target

Now to start up pump.io and make sure systemd starts it up
automatically after rebooting the server, run:

::

   $ sudo systemctl daemon-reload
   $ sudo systemctl start pump
   $ sudo systemctl enable pump

If everything seems to be working, you're done! Congratulations!

Upgrading pump.io
~~~~~~~~~~~~~~~~~

If you later wish to upgrade your pump.io server software, e.g. to the
latest version on npm (or even to git master), you can do something
like the following. (Note: you might want to backup your database
first using ``mongodump``, see the migration notes below.)

First, stop pump.io and switch to the pumpio user.

::

   $ sudo systemctl stop pump

Next, install the latest version with npm:
   
::

   $ sudo npm install -g pump.io

It's a good idea to update the database stuff at the same time:

::
   
   $ sudo npm install -g databank-mongodb

If you're unsure, especially after a big upgrade, you can always test
it first directly by running ``sudo -u pumpio pump`` as above. If all
seems well you can Ctrl-C and start it for real.

::
   
   $ sudo systemctl start pump
    
Site migration
~~~~~~~~~~~~~~

If you ever want to migrate to another server, you need to copy your
MongoDB contents and the upload directory. Something like this:

::

    $ mongodump
    $ rsync -var dump new.server.com:
    $ rsync /srv/pumpio/uploads new.server.com:

    $ ssh new.server.com
    $ mongorestore
    $ mv uploads to.correct.place
