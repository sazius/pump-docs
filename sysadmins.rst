For system administrators
=========================


Installation instructions
-------------------------

* `Debian <sysadmins-debian.html>`_

See also the `Sysadmin FAQ <sysadmins-faq.html>`_.
